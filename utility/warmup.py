#!/usr/bin/python

# Script per la configurazione del cluster zookeeper, da lanciare al primo avvio sulle tre instanze ec2

from socket import *
from threading import Thread
import boto, time, threading, urllib2, sys
from boto.s3.connection import OrdinaryCallingFormat

PATH_LINK = "https://s3.amazonaws.com/chatter.bucket/"
BUCKET_NAME = "chatter.bucket"
FILE_NAME = "ip_netloader"

HOST = gethostname()
PORT = 20000
data = []
message = []
WAITING = 1
LOCAL_IP = "127.0.0.1"
LOCAL_HOSTNAME = ""
NETLOADER_IP = "127.0.0.1"
PATH = "/home/ubuntu/"
CONF = "server/conf/"
DATA = "data/"

def recvMsg(c):
	f = c.makefile()
	length = ord(f.read(1))
	return f.read(length)
	
def sendMsg(c, msg):
	f = c.makefile()
	f.write(chr(len(msg))+msg)
	f.flush()

def writeMyid(myid):
	f = open(PATH+DATA+'myid','w')
	f.write(str(myid))
	f.close()

def writeConf(local_data):
	f = open(PATH+CONF+'zoo.cfg.dynamic','w')
	for elem in local_data:
		if LOCAL_HOSTNAME in elem:
			tmp = elem.split(".")[1]
			myid = tmp.split("=")[0]
		f.write(str(elem)+"\n")
	f.close()
	writeMyid(myid)

def local_warmup():
	writeConf(message)
	print "[OK]"
	
def warmup():
	HOST = NETLOADER_IP
	client_socket = socket(AF_INET, SOCK_STREAM)
	client_socket.settimeout(10000)
	print "[OK]"
	trying = 1
	done = 30
	while trying <= done:
		try:
			client_socket.connect((HOST, PORT))
			sendMsg(client_socket, LOCAL_HOSTNAME)
			break
		except:
			print "Server offline, retrying... (" + str(done-trying) + ")"
			trying += 1
			time.sleep(1)
	if trying > done:
		print "[ERROR] Server offline. Terminating..."
		sys.exit()
	print "Connected to the server"
	received = []
	while len(received) < 3:
		received.append(recvMsg(client_socket))
	writeConf(received)
	sendMsg(client_socket, "OK")
	print "Data received"

def receiverThread(client_socket, addr):
	global WAITING
	thread_name = str(threading.current_thread().getName())
	print "\tNew connection accepted (" + thread_name + ")"
	data.append(recvMsg(client_socket))
	while WAITING:		
		time.sleep(0.1)
	for elem in message:
		sendMsg(client_socket, elem)
	if (recvMsg(client_socket) == "OK"):
		print "\tWarmup completed, closing (" + thread_name + ")"
	else:
		print "\tWarmup interrupted, closing (" + thread_name + ")"

def openSocket():
	ADDR = (HOST, PORT)
	sock = socket(AF_INET, SOCK_STREAM)
	sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	sock.bind(ADDR)
	sock.listen(2)
	return sock
	
def netloader():
	global WAITING
	server_socket = openSocket()
	print "[OK]"
	print "Waiting connections..."
	data.append(LOCAL_HOSTNAME)
	run = 0
	threads = []
	while run < 2:
		client_sock, addr = server_socket.accept()
		threads.append(Thread(name="T"+str(run+1), target=receiverThread, args=(client_sock, addr)))
		threads[-1].start()
		run += 1
	while len(data) < 3:
		time.sleep(1)
	myid = 1
	for elem in data:
		message.append("server."+str(myid)+"="+data[myid-1]+":"+str(2880+myid)+":"+str(3880+myid)+":participant;0.0.0.0:2180")
		myid += 1
	WAITING = 0
	for thread in threads:
		thread.join()
	server_socket.close()

def uploadFile(local_name, cloud_name):
	s3 = boto.connect_s3(calling_format=OrdinaryCallingFormat())
	bucket = s3.create_bucket(BUCKET_NAME)
	key = bucket.new_key(cloud_name)
	key.set_contents_from_filename(local_name)
	key.set_acl('public-read')

if __name__=='__main__':
	try:
		response = urllib2.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4")
		LOCAL_IP = response.read()
	except:
		print "No Amazon response. Terminating..."
		sys.exit()
	f = open(PATH+"ip_local", 'w')
	f.write(LOCAL_IP)
	f.close()
	uploadFile(PATH+"ip_local", FILE_NAME)
	print "Waiting ip..."
	time.sleep(10)
	response = urllib2.urlopen(PATH_LINK+FILE_NAME)
	for line in response:
		NETLOADER_IP = line
	try:
		response = urllib2.urlopen("http://169.254.169.254/latest/meta-data/hostname")
		LOCAL_HOSTNAME = response.read()
	except:
		print "No Amazon response. Terminating..."
		sys.exit()
	if NETLOADER_IP == LOCAL_IP:
		print "Launching Netloader...\t",
		netloader()
		print "Setting Local Warmup...\t",
		local_warmup()
	else:
		print "Launching Warmup...\t",
		warmup()
	print "Terminating with success...\n"
