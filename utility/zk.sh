#!/bin/bash

case "$1" in
    start|1)
        server/bin/zkServer.sh start
        ;;
    stop|2)
        server/bin/zkServer.sh stop
        ;;
    check|status|3)
        server/bin/zkServer.sh status
        ;;
    restart|4)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage: $0 start|stop|check|status|restart"
        exit 1
esac
exit 0
