package updateMediaFiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteItemResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.amazonaws.services.dynamodbv2.util.TableUtils.TableNeverTransitionedToStateException;

public class DynamoDBUtils {
	
	private AmazonDynamoDBClient dynamoDBClient;
	private String TABLE_NAME;
	private String time;
	
	@SuppressWarnings("deprecation")
	public DynamoDBUtils(String tableName) {
		TABLE_NAME = tableName;
		dynamoDBClient = new AmazonDynamoDBClient(new InstanceProfileCredentialsProvider());
	}
	
	/*Creates a createTableRequest*/
	private CreateTableRequest createTableRequest() {
		CreateTableRequest request;
		
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
		attributeDefinitions.add(new AttributeDefinition().withAttributeName("User").withAttributeType("S"));
		attributeDefinitions.add(new AttributeDefinition().withAttributeName("Timeout").withAttributeType("N"));
		
		ArrayList<KeySchemaElement> tableKeySchema = new ArrayList<KeySchemaElement>();
		tableKeySchema.add(new KeySchemaElement().withAttributeName("User").withKeyType(KeyType.HASH));
		tableKeySchema.add(new KeySchemaElement().withAttributeName("Timeout").withKeyType(KeyType.RANGE));
		
		request = new CreateTableRequest()
				.withTableName(TABLE_NAME)
				.withKeySchema(tableKeySchema)
				.withAttributeDefinitions(attributeDefinitions)
				.withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(5L).withWriteCapacityUnits(5L));
		
		return request;
	}
	
	/*Creating the DB Table with userName as PrimaryKey*/
	public void createTable() {
		try {
			System.out.println("Creating table " + TABLE_NAME + ".");
			TableUtils.createTableIfNotExists(dynamoDBClient, createTableRequest());
			TableUtils.waitUntilActive(dynamoDBClient, TABLE_NAME);
			System.out.println("Table " + TABLE_NAME + " active.");
		} catch (TableNeverTransitionedToStateException e) {
			System.out.println("Error in createTable() -> waitUntilActive() - TableNeverTransitionedToStateException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Error in createTable() -> waitUntilActive() - InterruptedException");
			e.printStackTrace();
		}
	}

	
	/*Retrieving the list of items with timeout<=now*/
	public List<Map<String, AttributeValue>> scanTableForExpiredItems() {
		Date date = new Date();
		String sTime = String.valueOf(date.getHours()) + "." + String.valueOf(date.getMinutes());
		time = sTime;
		System.out.println("Scanning for item with timeout less than " + sTime);
		
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
		expressionAttributeValues.put(":val", new AttributeValue().withN(sTime));
		
		ScanRequest request = new ScanRequest()
				.withTableName(TABLE_NAME)
				.withFilterExpression("Timeout <= :val")
				.withExpressionAttributeValues(expressionAttributeValues);
		
		ScanResult result = dynamoDBClient.scan(request);
		List<Map<String, AttributeValue>> items = result.getItems();
		return items;
	}
	
	/*Deletes the result's items*/
	public void deleteItems(List<Map<String, AttributeValue>> items) {
		
		for (Map<String, AttributeValue> item : items) {
			
			String timeoutH = item.get("Timeout").getN().split("[.]")[0];
			if (time.split("[.]")[0].equals("23") && timeoutH.equals("00"))
				continue;
			
			System.out.println("Deleting item " + item.get("User").getS() + " - " + item.get("URL").getS() + " - " + item.get("Timeout").getN());
			
			Map<String, AttributeValue> map = new HashMap<>();
			map.put("User", item.get("User"));
			map.put("Timeout", item.get("Timeout"));
			
			DeleteItemRequest request = new DeleteItemRequest()
					.withTableName(TABLE_NAME)
					.withKey(map);
			DeleteItemResult deleteResult = dynamoDBClient.deleteItem(request);
			System.out.println("DeleteResult " + deleteResult);
		}
		
		time = null;
	}
	
	/*Returns the list of files to be deleted from S3*/
	public List<String> getFilesToDelete(List<Map<String, AttributeValue>> items) {
		List<String> fileNames = new ArrayList<>();
		
		for (Map<String, AttributeValue> item : items) {
			
			String timeoutH = item.get("Timeout").getN().split("[.]")[0];
			if (time.split("[.]")[0].equals("23") && timeoutH.equals("00"))
				continue;
			
			fileNames.add(getFileName(item.get("URL").getS()));
		}
		System.out.println(fileNames.size() + " files and entries found.");
		return fileNames;			
	}
	
	
	/*Returns the name of the file from the URL*/
	private String getFileName(String URL) {
		String delims = "[/]";
		String[] parts = URL.split(delims);
		return parts[parts.length-1];
	}
	
}
