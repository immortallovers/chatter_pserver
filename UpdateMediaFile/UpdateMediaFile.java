package updateMediaFiles;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class UpdateMediaFile {
	
	private static DynamoDBUtils dbUtils;
	private static S3Utils s3Utils;
	private static String BUCKET_NAME;
	private static String TABLE_NAME;
	private static int delay = 900000;
	
	/*Loads the S3 bucket and DynamoDB names from a config file*/
	private static void loadConfig() {
		BufferedReader bufReader = null;
		FileReader fileReader = null;
		String file = "system.config";
		String read = null;
		try {
			fileReader = new FileReader(file);
			bufReader = new BufferedReader(fileReader);
			if (bufReader != null) {
				while ((read = bufReader.readLine()) != null) {
					String[] parts = read.split("[=]");
					switch (parts[0]) {
						case "bucket":
							BUCKET_NAME = parts[1];
							break;
						case "table":
							TABLE_NAME = parts[1];
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Cannot find config file - loadConfig");
			System.exit(1);
		} catch (IOException io) {
			System.out.println("IOException reading config file - loadConfig");
			System.exit(1);
		}
		finally {
				try {
					if (bufReader != null)
						bufReader.close();
					if (fileReader != null)
						fileReader.close();
					
				} catch (IOException e) {
					System.out.println("IOException closing credential file - loadConfig");
				}
		}
	}

	public static void main(String[] args) {
		
		System.out.println("Loading system configurations.");
		loadConfig();
		
		System.out.println("Setting AWS clients.");
		dbUtils = new DynamoDBUtils(TABLE_NAME);
    	s3Utils = new S3Utils(BUCKET_NAME);		
		
		dbUtils.createTable();
		s3Utils.createBucket();
		
		while(true) {
			List<Map<String, AttributeValue>> items = dbUtils.scanTableForExpiredItems();
			List<String> fileNames = dbUtils.getFilesToDelete(items);
			
			for (String s : fileNames) {
				s3Utils.deleteFile(s);
			}
			
			dbUtils.deleteItems(items);
			
			System.out.println("Sleeping for 15 minutes.");
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				System.out.println("Error while sleeping");
				e.printStackTrace();
			}
		}
		
				
	}

}
