package updateMediaFiles;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class S3Utils {

	private static AmazonS3 s3Client;
	private static String BUCKET_NAME;
	
	@SuppressWarnings("deprecation")
	public S3Utils(String bucketName) {
		BUCKET_NAME = bucketName;
		s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new InstanceProfileCredentialsProvider()).build();
	}
	
	/*Creates Bucket*/
	public void createBucket() {
		try {
			System.out.println("Creating bucket " + BUCKET_NAME + ".");
			s3Client.createBucket(BUCKET_NAME);
			System.out.println("Bucket " + BUCKET_NAME + " created.");
		} catch(AmazonServiceException a) {
			System.out.println("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was "
					+ "rejected with an error responsefor some reason.");
			System.out.println(a.getMessage() + " - " + a.getErrorCode() + " - " + a.getErrorType());
		} catch (SdkClientException s) {
			System.out.println("Caught an AmazonClientException, which means the client encountered an internal error while "
					+ "trying to communicate with S3, such as not being able to access the network.");
			System.out.println(s.getMessage());
		}
	}
	
	/*Deletes files with KEY = key (its name)*/
	public void deleteFile(String key) {
		s3Client.deleteObject(BUCKET_NAME, key);
		System.out.println("File " + key + " deleted.");
	}
	
}
