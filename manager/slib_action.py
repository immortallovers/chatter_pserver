#!/usr/bin/python

# libreria action per il server

from kazoo.client 			import KazooState
from kazoo.client 			import KazooClient
from kazoo.recipe.watchers	import ChildrenWatch
from socket 				import *
from slib_dynamodb 			import *
from slib_storageS3			import *
from lib_queue  			import *
from lib_utility  			import *
from manager				import *
import os, sys, thread, time

LIMIT = 8192
DYNAMO = DynamoDB()
	
def authOk(sock):
	sendMsg(sock, fuseCMD(cmd_AUTH, OK))
	
def authKo(sock):
	sendMsg(sock, fuseCMD(cmd_AUTH, KO))
	
def authNo(sock):
	sendMsg(sock, fuseCMD(cmd_AUTH, NO))
	
def signOk(sock):
	sendMsg(sock, fuseCMD(cmd_C_ID, OK))
	
def signNo(sock):
	sendMsg(sock, fuseCMD(cmd_C_ID, NO))
	
def cntcOk(sock, u):
	sendMsg(sock, fuseCMD(cmd_CNTC, OK, u))
	
def cntcNo(sock, u):
	sendMsg(sock, fuseCMD(cmd_CNTC, NO, u))
	
def grpcOk(sock, g):
	sendMsg(sock, fuseCMD(cmd_GRPC, OK, g))
	
def grpcNo(sock, g):
	sendMsg(sock, fuseCMD(cmd_GRPC, NO, g))
	
def grpcNw(sock, g):
	sendMsg(sock, fuseCMD(cmd_GRPC, NW, g))
	
def grpuOk(sock, g, u):
	sendMsg(sock, fuseCMD(cmd_GRPU, OK, g, u))
	
def grpuNo(sock, g, u):
	sendMsg(sock, fuseCMD(cmd_GRPU, NO, g, u))
	
def grpuNa(sock, g, u):
	sendMsg(sock, fuseCMD(cmd_GRPU, NA, g, u))
	
def	grpadd(sock, g, u):
	sendMsg(sock, fuseCMD(cmd_GRPA, g, u))
	
def exitOk(sock):
	sendMsg(sock, cmd_EXIT)
	
def fileOk(sock, u, n):
	sendMsg(sock, fuseCMD(cmd_FILE, OK, u, n))
	
def fileNo(sock, u, n):
	sendMsg(sock, fuseCMD(cmd_FILE, NO, u, n))

def getAction(foo, this_user, args):
	if this_user.getUsername() != "" and (foo == "C_ID" or foo == "AUTH"):
		getArguments(this_user.getSocket(), 2)
		printD("Utente loggato, ignoro")
		if foo == "C_ID":
			confirmSign(this_user.getSocket())
			return args[1]
		elif foo == "AUTH":
			confirmAuth(this_user.getSocket())
			return args[1]
	else:
		return globals()[foo](*args)

def saveNewLinkOther(kc, this_user, new_node_path, other, save=1):
	this_node = fuse("chatter/", new_node_path)
	if this_user.getNameToQueue(other) == "":
		this_queue = Queue(kc, this_node, this_user.getUsername(), other)
		this_user.addOther(other)
		this_user.addNodePath(this_node)
		this_user.addQueueName(this_queue)
		this_user.addRxWatch(ChildrenWatch(kc, this_node, this_user.getWatchNode(), True, True))
		this_user.addNameToQueue(other, this_queue)
		this_user.addPathToQueue(this_node, this_queue)
		if save:
			DYNAMO.saveUserData(this_user)
	return this_user
	
def saveNewLinkGroup(kc, this_user, group, save=1):
	this_node = fuse("chatter/", group)
	if this_user.getNameToQueue(group) == "":
		this_queue = Queue(kc, this_node, this_user.getUsername(), group)
		this_user.addGroup(group)
		this_user.addNodePath(this_node)
		this_user.addQueueName(this_queue)
		this_user.addRxWatch(ChildrenWatch(kc, this_node, this_user.getWatchNode(), True, True))
		this_user.addNameToQueue(group, this_queue)
		this_user.addPathToQueue(this_node, this_queue)
		if save:
			DYNAMO.saveUserData(this_user)
	return this_user
	
def restoreUser(kc, this_user):
	new_user = DYNAMO.loadUserData(this_user)
	for other in new_user.getOthers():
		list_user = [new_user.getUsername(), other]
		new_node_path = fuse(min(list_user), "_", max(list_user))
		saveNewLinkOther(kc, new_user, new_node_path, other, 0)
	for group in new_user.getGroups():
		saveNewLinkGroup(kc, new_user, group, 0)
	node_recv = new_user.getMsgRxs()
	for elem in new_user.getQueueNames():
		valid, path_node, list_node = elem.getChildren()
		if valid:
			for node in list_node:
				tmp = fuse(path_node, "/", node)
				print tmp, node_recv
				if tmp not in node_recv:
					new_user.getWatchNodeOffline()(node, path_node)
	return new_user
	
def C_ID(kazoo_client, this_user):
	username, password = getArguments(this_user.getSocket(), 2)
	username = username.lower()
	node_users = fuse("userslist/", username)
	if not kazoo_client.exists(node_users):
		kazoo_client.create(node_users)
		kazoo_client.set(node_users, password)
		signOk(this_user.getSocket())
		this_user.setUsername(username)
		DYNAMO.saveUserData(this_user)
		print fuse("Nuovo utente ", username, " registrato")
		this_queue = Queue(kazoo_client, node_users, this_user.getUsername(), this_user.getUsername())
		this_user.addQueueName(this_queue)
		this_user.addRxWatch(ChildrenWatch(kazoo_client, node_users, this_user.getWatchRequest(), True, True))
	else:
		signNo(this_user.getSocket())
	return this_user

def AUTH(kazoo_client, this_user):
	username, password = getArguments(this_user.getSocket(), 2)
	username = username.lower()
	node_users = fuse("userslist/", username)
	try:
		kazoo_client.exists(node_users)
		username_exists = 1
		print fuse(password, " == ", kazoo_client.get(node_users)[0], ":", password== kazoo_client.get(node_users)[0])
	except:
		username_exists = 0
	if username_exists and password == kazoo_client.get(node_users)[0]:
		authOk(this_user.getSocket())
		this_user.setUsername(username)
		this_user = restoreUser(kazoo_client, this_user)
		print fuse("Accesso dell'utente ", this_user.getUsername())
	else:
		authNo(this_user.getSocket())
	return this_user

def CNTC(kazoo_client, this_user):
	other = getArguments(this_user.getSocket(), 1).lower()
	if kazoo_client.exists(fuse("userslist/", other)):
		list_user = [this_user.getUsername(), other]
		new_node_path = fuse(min(list_user), "_", max(list_user))
		this_user = saveNewLinkOther(kazoo_client, this_user, new_node_path, other)
		cntcOk(this_user.getSocket(), other)
	else:
		cntcNo(this_user.getSocket(), other)
	return this_user

def FILE(kazoo_client, this_user):
	receiver_name, f_name, length = getArguments(this_user.getSocket(), 3)
	sender_name = this_user.getUsername()
	if receiver_name in this_user.getGroups():
		sender_name = fuse(sender_name, "_", receiver_name)
	f_dir = 'manager/recvS/'
	file_name = fuse(f_dir, sender_name, f_name)
	residual = int(length)
	f_chunks = []
	while residual > LIMIT:
		chunk = this_user.getSocket().recv(LIMIT)
		while len(chunk) < LIMIT:
			chunk += this_user.getSocket().recv(LIMIT-len(chunk))
		f_chunks.append(chunk)
		residual -= LIMIT
	length_chunks = len(f_chunks)*LIMIT + residual
	chunk = this_user.getSocket().recv(residual)
	while len(chunk) < residual:
		chunk += this_user.getSocket().recv(residual-len(chunk))
	f_chunks.append(chunk)
	if length_chunks == int(length):
		f = open(file_name, 'wb')
		for chunk in f_chunks:
			f.write(chunk)
		f.close()
	uploadFile(file_name, fuse(sender_name, f_name))
	file_link = getFileLink(fuse(sender_name, f_name))
	this_queue = this_user.getNameToQueue(receiver_name)
	valid = False
	if this_queue != "":
		valid, node_set = this_queue.writeLink(fuse(sender_name, "|", file_link))
		try:
			os.remove(file_name)
		except:
			pass	
	if valid:
		try:
			timeout = convertLMTimeDB(node_set.last_modified)	
		except:
			timeout = "00.00"
		db_file = DynamoDB()
		print fuse("salvataggio link su dynamo: ", db_file.saveLink(this_user, timeout, file_link))
		fileOk(this_user.getSocket(), receiver_name, f_name)
		print "----- File Ricevuto"
	else:
		fileNo(this_user.getSocket(), receiver_name, f_name)
		print "----- Errore nella ricezione del file"
	return this_user
	
def GRPC(kazoo_client, this_user):
	local_data = getArguments(this_user.getSocket(), 1)
	if kazoo_client.exists(fuse("chatter/", local_data)):	#se esiste, prova ad accedere
		users = kazoo_client.get(fuse("chatter/", local_data))
		if this_user.getUsername() in users[0]:
			grpcOk(this_user.getSocket(), local_data)
			this_user = saveNewLinkGroup(kazoo_client, this_user, local_data)
		else:
			grpcNo(this_user.getSocket(), local_data)
	else:													#se non esiste, crealo
		kazoo_client.create(fuse("chatter/", local_data))
		kazoo_client.set(fuse("chatter/", local_data), fuse(this_user.getUsername(),"|"))
		this_user = saveNewLinkGroup(kazoo_client, this_user, local_data)
		grpcNw(this_user.getSocket(), local_data)
	return this_user

def GRPU(kazoo_client, this_user):
	group, name = getArguments(this_user.getSocket(), 2)	
	if kazoo_client.exists(fuse("chatter/", group)):		#se esiste, controlla se ne sei amministratore
		users = kazoo_client.get(fuse("chatter/", group))[0]
		if this_user.getUsername() == users.split("|")[0]:
			users += fuse(name, "|")
			kazoo_client.set(fuse("chatter/", group), users)
			grpuOk(this_user.getSocket(), group, name)
			node_request = fuse("userslist/", name, "/request_", group, "|", this_user.getUsername())
			kazoo_client.create(node_request)
			kazoo_client.set(node_request, fuse(group, "|", this_user.getUsername()))
		else:
			grpuNa(this_user.getSocket(), group, name)
	else:													#se non esiste, errore
		grpuNo(this_user.getSocket(), group, name)
	return this_user

def EXIT(kazoo_client, this_user):
	exitOk(this_user.getSocket())
	this_user.setDisconnect()
	return this_user

def TX_M(kazoo_client, this_user, data, addr):	# a zookeeper
	user, message = readMsgServer(data, this_user)
	print [user, message]
	printS("\nTX per [", user, "]")
	try:
		local_queue = this_user.getNameToQueue(user)
		if local_queue != "":
			valid_write, node_set = this_user.getNameToQueue(user).writeMessage(message)
			if valid_write:
				lm_time = node_set.last_modified
				if user in this_user.getGroups():
					user = this_user.getUsername()+"_"+user
				sendTxMsg(this_user.getSocket(), user, convertLMTime(lm_time), message)
			else:
				print "Errore in sendTxMsg\n"
		else:
			print fuse(user, " non registrato")
	except KeyError:
		print fuse(user, " non registrato")
	return this_user

def RX_M(kazoo_client, this_user, data, addr):	# dal client
	printS("\nRX da [", addr, "]")
	try:
		this_user = getAction(data, this_user, (kazoo_client, this_user))
	except KeyError:
		declineAuth(this_user.getSocket())
		print "Scollegato"
		this_user.setDisconnect()
	return this_user
