from kazoo.client import KazooState
from kazoo.client import KazooClient

from socket import gethostname

CHATTER = 1
USERSLIST = 1

def watchAction(action, args=[]):
	try:
		if len(args) == 2:
			action(args[0], recursive=args[1])
		elif len(args) == 1:
			action(args[0])
		else:
			action()
		print "[DONE]",
	except:
		print "[FAIL]",

def resetZookeeper(ip):
	zk = KazooClient(ip)
	watchAction(zk.start)
	print "Contacting Zookeeper"

	node = []
	if CHATTER:
		node.append("chatter")
	if USERSLIST:
		node.append("userslist")
		
	for elem in node:
		watchAction(zk.delete, (elem, True))
		print "Node " + elem + " deleting"
		watchAction(zk.create, (elem,))
		print "Node " + elem + " creating"

	zk.stop()
