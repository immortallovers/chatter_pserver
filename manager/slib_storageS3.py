#!/usr/bin/python
# -*- coding: utf-8 -*-

from boto.s3.connection import OrdinaryCallingFormat
import boto

PATH_LINK = "https://s3.amazonaws.com/chatter.bucket/"
BUCKET_NAME = "chatter.bucket"

def getFileLink(file_name):
	return str(PATH_LINK) + str(file_name)
	
def uploadFile(local_name, cloud_name):
	s3 = boto.connect_s3(calling_format=OrdinaryCallingFormat())
	bucket = s3.create_bucket(BUCKET_NAME)
	key = bucket.new_key(cloud_name)
	key.set_contents_from_filename(local_name)
	key.set_acl('public-read')
