#!/usr/bin/python

# libreria DynamoDB

import boto3, json, decimal, sys
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

TABLE_NAME = "Users_db"
TABLE_FILE = "chatter_MediaFile_Table"
DISACTIVATED = 0

def convert(data):
		tmp = []
		for elem in data:
			tmp.append(elem.encode('utf-8'))
		return tmp		
	
class DynamoDB:
	def __init__(self):
		self.resource = boto3.resource('dynamodb', region_name='eu-central-1')
		self.client = boto3.client('dynamodb', region_name='eu-central-1')

	def createTable(self):
		try:
			#data: 		Name 1' key, Type 1' key, Datatype 1' key		Name 2' key, Type 2' key, Datatype 2' key
			data = ["Username", "HASH", "S", "ID_user", "RANGE", "S"]	
			table = self.resource.create_table(TableName=TABLE_NAME,
				KeySchema = [{'AttributeName' : data[0], 'KeyType' : data[1]}, {'AttributeName' : data[3], 'KeyType' : data[4]}],
				AttributeDefinitions = [{'AttributeName' : data[0], 'AttributeType' : data[2]}, {'AttributeName' : data[3], 'AttributeType' : data[5]}],
				ProvisionedThroughput = {'ReadCapacityUnits' : 10, 'WriteCapacityUnits' : 10})
			self.client.get_waiter('table_exists').wait(TableName=TABLE_NAME)
			return True
		except:
			return False
	
	def saveUserData(self, this_user):
		if DISACTIVATED:
			return True
		try:
			table = self.resource.Table(TABLE_NAME)
			table.put_item(Item = {'Username' : this_user.getUsername(), 'ID_user' : '0', 'others' : this_user.getOthers(), 'groups' : this_user.getGroups(), 'msgs_rx' : this_user.getMsgRxs()})
			return True
		except:
			return False
			
	def loadUserData(self, this_user):
		if DISACTIVATED:
			return this_user
		table = self.resource.Table(TABLE_NAME)
		try:
			response = table.get_item(Key={'Username': this_user.getUsername(),'ID_user': '0'})
			this_user.setOthers(convert(response['Item']['others']))
			this_user.setGroups(convert(response['Item']['groups']))
			this_user.setMsgRxs(convert(response['Item']['msgs_rx']))
		except:
			print "User not saved in Dynamo"
		return this_user

	def saveLink(self, this_user, timeout, n_file):
		if DISACTIVATED:
			return True
		try:
			table = self.resource.Table(TABLE_FILE)
			table.put_item(Item = {'User' : this_user.getUsername(), 'Timeout' : timeout, 'URL' : n_file})
			return True
		except:
			return False
