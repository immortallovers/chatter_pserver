#!/usr/bin/python

# s_manager.py - il server dialoga con zookeeper, inviando messaggi secondo un certo ordine e ricevendo i watcher in cambio

from kazoo.client 			import KazooState
from kazoo.client 			import KazooClient
from kazoo.recipe.watchers 	import ChildrenWatch
from socket 				import *
from lib_queue 				import *
from lib_utility 			import *
from slib_action 			import *
from slib_dynamodb 			import *
from slib_resetZK			import *
from slib_user	 			import *
import os, sys, thread, threading, time

SERVER_PRINT = 1
DEBUG_PRINT = 1
HOST = gethostname()
MANAGER_PORT = 10000
BALANCER_PORT = 5000
IP_ZOOKEEPER = '127.0.0.1:2180'

def printD(*data):
	if DEBUG_PRINT:
		print fuse(*data)
	
def printS(*data):
	if SERVER_PRINT:
		print fuse(*data)

def confirmAuth(sock):
	sendMsg(sock, fuseCMD(cmd_AUTH, OK))
	
def declineAuth(sock):
	sendMsg(sock, fuseCMD(cmd_AUTH, KO))

def confirmSign(sock):
	sendMsg(sock, fuseCMD(cmd_C_ID, OK))	

def mergeUser(g, u):
	if g in this_user.getGroups():
		return fuse(u, "_", g)
	else:
		return fuse(g)

def listenerComm(local_sock, addr, run_event):
		
	def runKeep(keep_sock, local_run, this_user):
		time.sleep(WAIT_KEEPALIVE)
		while local_run.is_set():
			try:
				sendMsg(keep_sock, "KEEP")
			except:
				this_user.setDisconnect()
				sys.exit()
			time.sleep(WAIT_KEEPALIVE)
		sys.exit()
	
	def watchRequest(data, stat):
		if data and stat:
			for i in range(0, len(data)):
				try:
					parsed_text = data[i].split("_")[1]
					group_name, admin_name = parsed_text.split("|")
					grpadd(this_user.getSocket(), group_name, admin_name)
				except:
					print "Errore in watchRequest:", sys.exc_info()
					return this_user
				try:
					this_node = fuse("chatter/", group_name)
					if this_user.getNameToQueue(group_name) == "":
						this_queue = Queue(kazoo_client, this_node, this_user.getUsername(), group_name)
						this_user.addGroup(group_name)
						this_user.addNodePath(this_node)
						this_user.addQueueName(this_queue)
						this_user.addRxWatch(ChildrenWatch(kazoo_client, this_node, this_user.getWatchNode(), True, True))
						this_user.addNameToQueue(group_name, this_queue)
						this_user.addPathToQueue(this_node, this_queue)
						DYNAMO.saveUserData(this_user)
						print fuse("userslist/", data[i])
						this_user.getNameToQueue(group_name).deleteMessage(fuse("userslist/", data[i]))
				except:
					print sys.exc_info()
		return this_user
	
	def watchNodeOffline(data, stat):
		msgs_to_send = []
		link_to_send = []
		try:
			this_node = fuse(stat, "/", data)
			this_queue = this_user.getPathToQueue(stat)
			if this_queue != "":
				node_valid, data_readed = this_queue.readMessage(data)
				if not node_valid:
					return this_user
				msg_readed = data_readed[0]
				lm_time = data_readed[1].last_modified
				if data[0:5] != "file_":
					actual_user = ""
					for key, value in this_user.getNameToQueues().items():
						if this_queue == value:
							actual_user = fuse(key)
							if key in this_user.getGroups():
								actual_user = fuse(data.split("_")[0], "_", key)
					msgs_to_send.append([lm_time, actual_user, msg_readed, this_node])
				else:
					parsed = msg_readed.split("|")
					link_to_send.append([lm_time, parsed[0], parsed[1], this_node])
			else:
				return this_user
		except:
			print "Errore in watchNode (interno):", sys.exc_info()
		msgs_to_send.sort()
		for msg in msgs_to_send:
			try:
				sendRxMsg(this_user.getSocket(), msg[1], convertLMTime(msg[0]), msg[2])
				if "_" in msg[1]:
					this_user.addMsgRx(this_node)
				else:
					this_user.getNameToQueue(msg[1]).deleteMessage(msg[3])
			except:
				print fuse(this_user.getUsername(), "Errore in msgs_to_send:"), sys.exc_info()
				
		link_to_send.sort()
		for msg in link_to_send:
			try:
				sendFileServer(this_user.getSocket(), msg[0], msg[1], msg[2])
				this_user.addMsgRx(msg[3])
			except:
				print "Errore in link_to_send:", sys.exc_info()

	def watchNode(data, stat):
		msgs_to_send = []
		link_to_send = []
		if data and stat:
			for i in range(0, len(data)):
				try:
					if this_user.getUsername() in data[i]:
						continue
					path = stat[2][1:]
					this_node = fuse(path, "/", data[i])
					this_queue = this_user.getPathToQueue(path)
					print this_node
					if (not this_node in this_user.getMsgRxs()) and this_queue != "":
						node_valid, data_readed = this_queue.readMessage(data[i])
						if not node_valid:
							continue
						msg_readed = data_readed[0]
						lm_time = data_readed[1].last_modified
						if data[i][0:5] != "file_":
							actual_user = ""
							for key, value in this_user.getNameToQueues().items():
								if this_queue == value:
									actual_user = fuse(key)
									if key in this_user.getGroups():
										actual_user = fuse(data[i].split("_")[0], "_", key)
							msgs_to_send.append([lm_time, actual_user, msg_readed, this_node])
						else:
							parsed = msg_readed.split("|")
							link_to_send.append([lm_time, parsed[0], parsed[1], this_node])
					else:
						continue
				except:
					print "Errore in watchNode (interno):", sys.exc_info()
		
		msgs_to_send.sort()
		for msg in msgs_to_send:
			try:
				sendRxMsg(this_user.getSocket(), msg[1], convertLMTime(msg[0]), msg[2])
				if "_" in msg[1]:
					this_user.addMsgRx(this_node)
				else:
					this_user.getNameToQueue(msg[1]).deleteMessage(msg[3])
			except:
				print fuse(this_user.getUsername(), "Errore in msgs_to_send:"), sys.exc_info()
				
		link_to_send.sort()
		for msg in link_to_send:
			try:
				sendFileServer(this_user.getSocket(), msg[0], msg[1], msg[2])
				this_user.addMsgRx(msg[3])
			except:
				print "Errore in link_to_send:", sys.exc_info()

	this_user = User("", watchNode, watchNodeOffline, watchRequest, local_sock)
	local_run = threading.Event()
	local_run.set()
	t_keep = threading.Thread(target=runKeep, args=(local_sock, local_run, this_user))
	t_keep.start()
	kazoo_client = KazooClient(hosts=IP_ZOOKEEPER)
	kazoo_client.start()
	working = 1
	while run_event.is_set():
		if this_user.isDisconnect():
			break
		try:
			data = recvCmd(local_sock)
		except timeout:
			continue
		if this_user.getUsername() != "":
			try:
				printD("\n[", this_user.getUsername(), "]: ", data)
				this_user = getAction(data, this_user, (kazoo_client, this_user))
				
			except KeyError:
				this_user = TX_M(kazoo_client, this_user, data, addr)
		else:
			this_user = RX_M(kazoo_client, this_user, data, addr)
	
	if DYNAMO.saveUserData(this_user):
		print "Context of user saved\n"
	for rx_watch in this_user.getRxWatchs():
		rx_watch._stopped = True
	printS("\nClosing Keep Thread...")
	t_keep.join()
	printS("\nConnection with ", addr, " terminated...")
	sys.exit()

def initSock(host, port, t=0):
	sock = socket(AF_INET, SOCK_STREAM)
	sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	sock.bind((host, port))
	sock.listen(5)
	return sock

def listenerLB(lb_sock, run_event):
	printD("\nWaiting load balancer requests...")
	while run_event.is_set():
		try:
			req_sock, addr = lb_sock.accept()
		except:
			sys.exit()
		req_sock.close()
	sys.exit()

if __name__=='__main__':	
	WAIT_KEEPALIVE = 30
	run_event = threading.Event()
	run_event.set()
	lb_sock = initSock(HOST, BALANCER_PORT)
	t_lb = threading.Thread(target=listenerLB, args=(lb_sock, run_event))
	t_lb.start()
	mydb = DynamoDB()
	print mydb.createTable()
	resetZookeeper(IP_ZOOKEEPER)
	server_sock = initSock(HOST, MANAGER_PORT)
	threads = []
	socks = []
	try:
		printS("\nServer ", HOST, ":", MANAGER_PORT, " - waiting connections...")
		while True:
			client_sock, addr = server_sock.accept()
			client_sock.settimeout(5)
			printS("\nNew connection: ", addr)
			socks.append(client_sock)
			threads.append(threading.Thread(target=listenerComm, args=(client_sock, addr, run_event)))
			threads[-1].start()
	except KeyboardInterrupt:
		printS("\nClosing threads...")
		run_event.clear()
		WAIT_KEEPALIVE = 1
		lb_sock.shutdown(SHUT_RDWR)
		t_lb.join()
		printS("\nSocks closed...") 
		for thread in threads:
			thread.join()
	printS("Done. Goodbye!\n")
