#!/usr/bin/python

# libreria Queue

import sys, time

class Queue:
	def __init__(self, new_kclient, new_path, new_tx, new_rx):
		self.kclient = new_kclient
		self.path = new_path
		self.my_user = new_tx
		self.other_user = new_rx
		self.writed = 0
		if not self.kclient.exists(self.path):
			try:
				self.kclient.create(self.path)
			except:
				pass

	def getChildren(self):
		if self.kclient.exists(self.path+"/"):
			children = self.kclient.get_children(self.path+"/")
			return True, self.path, children
		return False, "", []
	
	def readMessage(self, name):
		node_path = self.path+"/"+name
		try:
			if not self.kclient.exists(node_path):
				return False, []
			msg = self.kclient.get(node_path)
			count = 0
			while len(msg[0]) == 0:
				time.sleep(0.1)
				msg = self.kclient.get(node_path)
				count += 1
				if count == 5:
					return False, []
			return True, msg
		except:
			print "Errore in readMessage:",sys.exc_info()
			return False, []
		
	def writeMessage(self, msg):
		node_path = str(self.path) + "/" + str(self.my_user) + "_" + str(self.writed)
		try:
			if not self.kclient.exists(node_path):
				self.kclient.create(node_path)
			node_set = self.kclient.set(node_path, msg)
			self.writed += 1
			return True, node_set
		except:
			print "Errore in writeMessage:", sys.exc_info()
			return False, ()
	
	def writeLink(self, msg):
		node_path = str(self.path) + "/file_" + str(self.my_user) + "_" + str(self.writed)
		try:
			if not self.kclient.exists(node_path):
				self.kclient.create(node_path)
			node_set = self.kclient.set(node_path, msg)
			self.writed += 1
			return True, node_set
		except:
			print "Errore in writeMessage:", sys.exc_info()
			return False, ()

	def deleteMessage(self, node_name):
		try:
			self.kclient.delete(node_name)
			self.writed = 0
		except:
			pass

