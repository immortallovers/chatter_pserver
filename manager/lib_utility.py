#!/usr/bin/python

# libreria con funzioni di utility comuni per client e server

import time, select, sys, threading
from Crypto.Cipher import AES
from socket import *

ERRDEBUG = 1
CRYPTED = 1
TIMEOUT = 600
cmd_AUTH = "AUTH"
cmd_C_ID = "C_ID"
cmd_CNTC = "CNTC"
cmd_EXIT = "EXIT"
cmd_FILE = "FILE"
cmd_GRPA = "GRPA"
cmd_GRPC = "GRPC"
cmd_GRPU = "GRPU"
OK = "OK"
KO = "KO"
NO = "NO"
NW = "NW"
NA = "NA"
		
KEY = "Trunksistor12345"
IV = "ImmortalLovers67"
BLOCK_SIZE = 16

pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
unpad = lambda s : s[0:-ord(s[-1])]
	
def sendMsg(c, msg, d=0): #
	c.sendall(msg)
	if d:
		limit = msg.rfind('|')
		if msg != "KEEP" and limit < len(msg)-1:
			msg = fuse(msg[:limit+1], ">Encrypted Message<\n")
		print fuse("\n----- Comando inviato ", msg, "\n")
	
def code(text):
	return unicode(text, "utf-8").encode('utf-8')
	
def encryptMsg(plaintext):
	if CRYPTED:
		cipher = AES.new(KEY, AES.MODE_CBC, IV)
		try:
			return cipher.encrypt(code(pad(plaintext)))
		except:
			print "Error on encryption", sys.exc_info()
			return False
	return plaintext
	
def decryptMsg(ciphertext):
	if CRYPTED:
		cipher = AES.new(KEY, AES.MODE_CBC, IV)
		try:
			return unpad(cipher.decrypt(ciphertext))
		except:
			print "Error on decryption", sys.exc_info()
			return False
	return ciphertext

def readMsgServer(data, this_user):
	user, n = getArguments(this_user.getSocket(), 2, 0)
	print fuse(data, user, " ", n, "\n\n\n\n\n")
	return fuse(data, user), fuse(recvMsg(this_user.getSocket(), int(n)))
	
def readMsgClient(sock):
	user, lm_time, length = getArguments(sock, 3)
	return user, lm_time, recvMsg(sock, int(length))

def getArguments(sock, n, x=0):
	args = []
	if x:
		jumpByte(sock)
	for i in range (0, n):
		data = ""
		temp = ""
		while data != "|":
			temp += data
			data = recvMsg(sock, 1)
		args.append(temp)
	if x:
		print args
	if n == 1:
		return args[0]
	return args
	
def recvCmd(c):
	return recvMsg(c, 4)
	
def recvMsg(c, length):
	data = ""
	count = 0
	while len(data) == 0:
		count += 1
		data = c.recv(length)
		if count > 1000:
			raise timeout
	return data

def jumpByte(c):
	c.recv(1)
	
def sendTextMsg(c, u, msg):
	msg = encryptMsg(msg)
	msg = str(u)+"|"+str(len(msg))+"|"+msg
	sendMsg(c, msg)
	
def sendTxMsg(c, u, t, msg):
	msg = fuse(fuseCMD("TX_M", u, t, len(msg)), msg)
	sendMsg(c, msg)

def sendRxMsg(c, u, t, msg):
	msg = fuse(fuseCMD("RX_M", u, t, len(msg)), msg)
	sendMsg(c, msg)

def sendFileServer(c, t, u, msg):
	now = time.time()
	if t+TIMEOUT > now:
		msg = fuseCMD("NFLE", u, msg)
	else:
		msg = fuseCMD("NFLE", u, "TIMEOUT")
	sendMsg(c, msg)
	
def err(data=""):
	return raw_input("\n"+str(data)+"Premi invio per terminare... ")

def convertLMTime(mtime):
	try:
		conv_time = time.strftime('%d:%m:%Y:%H:%M:%S', time.localtime(mtime))
	except:
		print sys.exc_info()
		conv_time = "00:00:00:00:00:00"
	return str(conv_time)

def convertLMTimeDB(mtime):
	try:
		conv_time = time.strftime('%H:%M', time.localtime(mtime))
	except:
		print sys.exc_info()
		conv_time = "00:00"
	return str(conv_time)

def fuse(*args):
	result = ""
	for string in args:
		result += str(string)
	return result

def fuseCMD(*args):
	result = ""
	for string in args:
		result += fuse(string, "|")
	return result

def checkMsg(d, m):
	data = []
	data = d.split("|")
	data_ret = []
	for i in range(0, m-1):
		data_ret.append(data.pop(0))
	data_ret.append("|".join(str(x) for x in data))
	return data_ret

def parseOpt(f_name):
	f_input = open(f_name, "r")
	f_read = f_input.read()
	lines = f_read.split("\n")
	user = lines[0].split(":")[1]
	others = lines[1].split(":")[1].split(";")
	num = int(lines[2].split(":")[1])
	groups = []
	if len(lines) > 4:
		groups = lines[3].split(":")[1].split(";")
	f_input.close()
	return user, others, num, groups

def printC(msg, num_user):
	init = '\033[91m'
	if num_user == 0:
		init = '\033[91m'
	elif num_user == 1:
		init = '\033[92m'
	elif num_user == 2:
		init = '\033[93m'
	elif num_user == 3:
		init = '\033[94m'
	else:
		init = '\033[95m'
	print init + str(msg) + '\033[0m' 
