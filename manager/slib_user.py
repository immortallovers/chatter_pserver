#!/usr/bin/python

# libreria User per manager.py

def fuse(*args):
	result = ""
	for string in args:
		result += str(string)
	return result

class User:
	def __init__(self, u, w, o, r, s):
		self.username = u
		self.node_path = []
		self.queue_name = []
		self.rx_watch = []
		self.others = []
		self.groups = []
		self.msgs_rx = []
		self.name_to_queue = {}
		self.path_to_queue = {}
		self.watchNode = w
		self.watchNodeOffline = o
		self.watchRequest = r
		self.sock = s
		self.disconnect = 0
			
	def getUsername(self):
		return self.username
			
	def setUsername(self, u):
		self.username = u
			
	def setNodePaths(self, n):
		self.node_path = n
		
	def addNodePath(self, n):
		if n not in self.node_path:
			self.node_path.append(n)
		
	def getNodePath(self, n):
		if len(self.node_path) > n:
			return self.node_path[n]
		else:
			return ""

	def getLastNodePath(self):
		if len(self.node_path) > 0:
			return self.node_path[-1]
		else:
			return ""

	def getNodePaths(self):
		return self.node_path
		
	def addQueueName(self, n):
		if n not in self.queue_name:
			self.queue_name.append(n)

	def getQueueName(self, n):
		if len(self.queue_name) > n:
			return self.queue_name[n]
		else:
			return ""
	
	def getQueueNames(self):
		return self.queue_name
	
	def getLastQueueName(self):
		if len(self.queue_name) > 0:
			return self.queue_name[-1]
		else:
			return ""
			
	def addRxWatch(self, n):
		if n not in self.rx_watch:
			self.rx_watch.append(n)

	def getRxWatch(self, n):
		if len(self.rx_watch) > n:
			return self.rx_watch[n]
		else:
			return ""

	def getRxWatchs(self):
		return self.rx_watch

	def setOthers(self, n):
		self.others = n
		
	def addOther(self, n):
		if n not in self.others:
			self.others.append(n)

	def getOther(self, n):
		if len(self.others) > n:
			return self.others[n]
		else:
			return ""

	def getLastOther(self):		
		if len(self.others) > 0:
			return self.others[-1]
		else:
			return ""

	def getOthers(self):
		return self.others
	
	def setGroups(self, n):
		self.groups = n
		
	def addGroup(self, n):
		if n not in self.groups:
			self.groups.append(n)

	def getGroup(self, n):
		if len(self.groups) > n:
			return self.groups[n]
		else:
			return ""

	def getLastGroup(self):		
		if len(self.others) > 0:
			return self.others[-1]
		else:
			return ""

	def getGroups(self):
		return self.groups
	
	def setMsgRxs(self, n):
		self.msgs_rx = n

	def addMsgRx(self, n):
		if n not in self.msgs_rx:
			self.msgs_rx.append(n)

	def getMsgRx(self, n):
		if len(self.msgs_rx) > n:
			return self.msgs_rx[n]
		else:
			return ""

	def getMsgRxs(self):
		return self.msgs_rx

	def addNameToQueue(self, u, q):
		if u not in self.name_to_queue:
			self.name_to_queue[u] = q

	def getNameToQueue(self, u):
		if u in self.name_to_queue:
			return self.name_to_queue[u]
		else:
			return ""

	def getNameToQueues(self):
		return self.name_to_queue
		
	def addPathToQueue(self, p, q):
		if p not in self.path_to_queue:
			self.path_to_queue[p] = q

	def getPathToQueue(self, p):
		if p in self.path_to_queue:
			return self.path_to_queue[p]
		else:
			return ""

	def getPathToQueues(self):
		return self.path_to_queue
		
	def getWatchNode(self):
		return self.watchNode

	def getWatchNodeOffline(self):
		return self.watchNodeOffline

	def getWatchRequest(self):
		return self.watchRequest

	def getSocket(self):
		return self.sock
	
	def setDisconnect(self):
		self.disconnect = 1
		
	def isDisconnect(self):
		return self.disconnect
	
	def getInfo(self):
		print fuse(self.getUsername(), "\n", self.getNodePaths(), "\n", self.getQueueNames(), "\n", self.getRxWatchs(), "\n",
					self.getOthers(), "\n", self.getGroups(), "\n", self.getMsgRxs(), "\n", self.getNameToQueues(), "\n",
					self.getPathToQueues(), "\n", self.getWatchNode(), "\n", self.getSocket(), "\n", self.isDisconnect(), "\n\n\n\n\n")
